# OpenML dataset: TEAMFIGHT-TACTICS-FATES-Challenger-EUW-Rank-Games

https://www.openml.org/d/43790

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
TFT
TFT is an 8-player free-for-all drafting tactics game in which the player recruits powerful champions, deploys them, and battles to become the last player standing. 
When acquired, a champion starts as a 1-Star. Three of the same 1-Star champion can be combined to create a stronger 2-Star champion. Three of the same 2-star champions can be combined to create an even stronger 3-Star champion. 
Champions have one origin and one class, with a few exceptions. Satisfying the origin or class conditions grants bonus effects to the entire team or to the champions of the respective origin or class. 
A champion can hold up to 3 items at a time.
Set 4
In Set 4: Fates, Chosen champions occasionally appear in a player's shop. Player can have at most one Chosen champion in the game. 
Chosen champions come with some extra power:

One of their traits (origin or class) is "Chosen" and counts as 2 of that trait.        
They gain 200 bonus health.
They also gain a unique bonus stat, based on the champion. (wiki)

Content
There are 14 columns, most of them are explained intuitively.
I'll explain 2 more complicated columns:
Traits column: json data - key: value
key: Trait name
value: Number of units with this trait
Units column: json data - key:value
key: Unit name
value: Dictionary 'items': items_array, 'stars': number of unit's stars
Inspiration
Most used items/champions
Most used items/champions in different versions
Matches with 3-stared 4/5 cost units

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43790) of an [OpenML dataset](https://www.openml.org/d/43790). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43790/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43790/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43790/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

